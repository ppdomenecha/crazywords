﻿using Newtonsoft.Json;
using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    //public Text emailField;
    //public Text passwordField;
    public WorldManager worldManager;

    public InputField emailField;
    public InputField passwordField;
    public TMP_Text invalidLogin;

    private class Auth
    {
        public string token;
        public string id;
    }

    public void SendLogin()
    {
        StartCoroutine(SendLogin(emailField.text, passwordField.text));
    }

    private IEnumerator SendLogin(string email, string password)
    {
        var request = new UnityWebRequest(WorldManager.API_BASE_URL + "/authentication", "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
        {
            email = email,
            password = password
        }));
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json, text/plain, */*");
        yield return request.SendWebRequest();

        while (!request.isDone) yield return new WaitForSeconds(0.1f);
        if (request.isHttpError && request.responseCode == 400)
        {
            invalidLogin.gameObject.SetActive(true);
        }
        else if (request.isNetworkError || request.isHttpError)
        {
            Application.Quit();
        }
        else
        {
            var token = JsonUtility.FromJson<Auth>(request.downloadHandler.text).token;
            yield return GetUserAuthenticated(token);
        }
    }

    private IEnumerator GetUserAuthenticated(string token)
    {
        var request = new UnityWebRequest(WorldManager.API_BASE_URL + "/authentication", "GET");
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json, text/plain, */*");
        request.SetRequestHeader("Authorization", "Bearer " + token);
        yield return request.SendWebRequest();

        while (!request.isDone) yield return new WaitForSeconds(0.1f);
        if (request.isNetworkError || request.isHttpError)
        {
            
            Application.Quit();
        }
        else
        {
            var userId = JsonUtility.FromJson<Auth>(request.downloadHandler.text).id;
            worldManager.SetCurrentUser(token + "{SEPARADOR}" + userId);
        }
    }
}
