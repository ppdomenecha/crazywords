﻿using System.Collections;

namespace Assets.Scripts.Tracker
{
    public interface IEventTracker
    {
        IEnumerator TrackEvent(Event eventToTrack);
    }
}
