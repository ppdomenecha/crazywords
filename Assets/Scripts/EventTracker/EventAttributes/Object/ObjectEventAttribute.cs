﻿namespace Assets.Scripts.Tracker.EventAttributes
{
    public class ObjectEventAttribute
    {
        public string id;
        public ObjectDefinitionEventAttribute definition;
    }
}