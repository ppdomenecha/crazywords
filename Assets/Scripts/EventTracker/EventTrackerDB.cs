﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Tracker
{
    public class EventTrackerDB : IEventTracker
    {
        private readonly string userToken;

        public EventTrackerDB(string userToken)
        {
            this.userToken = userToken;
        }

        public IEnumerator TrackEvent(Event eventToTrack)
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter());
            string jsonEvent = JsonConvert.SerializeObject(new { JsonEvent = JsonConvert.SerializeObject(eventToTrack, settings)}, settings);
            string gameplayId = eventToTrack.contextAttribute.contextActivities.parent[0].id
                .Substring(eventToTrack.contextAttribute.contextActivities.parent[0].id.LastIndexOf('/') + 1);
            yield return this.SendRequestWithBody(jsonEvent, gameplayId);
        }

        private IEnumerator SendRequestWithBody(string bodyJsonString, string gameplayId)
        {
            string url = WorldManager.API_BASE_URL + "/gameplays/" + gameplayId + "/events";
            var request = new UnityWebRequest(url, "POST");
            byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Access-Control-Allow-Origin", "*");
            request.SetRequestHeader("Access-Control-Allow-Methods", "*");
            request.SetRequestHeader("Allowed-Hosts", "*");
            request.SetRequestHeader("Accept", "application/json, text/plain, */*");
            request.SetRequestHeader("Authorization", "Bearer " + this.userToken);
            yield return request.SendWebRequest();

            while (!request.isDone) yield return new WaitForSeconds(0.1f);
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
                Application.Quit();
            }
            else
            {
                // Do nothing
            }
        }
    }
}
