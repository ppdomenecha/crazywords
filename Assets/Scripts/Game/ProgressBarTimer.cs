﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarTimer : MonoBehaviour
{
    private Image timeBar;
    private float elapsedTimeFromStart;
    private CheckIdleScript checkIdleScript;

    public float maxTime;
    public float leftTime;
    public TextMeshProUGUI timerText;
    public HeaderManager headerManagerScript;

    public void Start()
    {
        timeBar = GetComponent<Image>();
        elapsedTimeFromStart = 0.0f;
        Time.timeScale = 1;
        this.enabled = true;
        checkIdleScript = new CheckIdleScript();
    }

    public void Update()
    {
        if (this.headerManagerScript.IsGameFinished())
        {
            this.enabled = false;
            return;
        }

        this.elapsedTimeFromStart += Time.deltaTime;
        string minutes = ((int)(maxTime - elapsedTimeFromStart) / 60).ToString();
        string seconds = ((int)(maxTime - elapsedTimeFromStart) % 60).ToString();
        if (leftTime > 0)
        {
            leftTime -= Time.deltaTime;
            timeBar.fillAmount = 1f - leftTime / maxTime;
            timerText.text = "Tiempo restante: " + minutes + ":" + (seconds.Length == 1 ? "0" + seconds : seconds);
            checkIdleScript.UpdateTime(this.elapsedTimeFromStart);
            if (checkIdleScript.IsPlayerIdle())
            {
                headerManagerScript.ManageIdlePlayer();
            }
        }
        else
        {
            timerText.text = "Tiempo restante: 00:00";
            Time.timeScale = 0;
            headerManagerScript.TimesUp();
            this.enabled = false;
        }
    }

    public void ActionDetected()
    {
        this.checkIdleScript.RestartTime(this.elapsedTimeFromStart);
    }

    public void StopTimer()
    {
        this.enabled = false;
        Time.timeScale = 0;
    }

    public void ResumeTimer()
    {
        this.enabled = true;
        Time.timeScale = 1;
    }
}
