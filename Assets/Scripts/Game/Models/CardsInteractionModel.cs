﻿using UnityEngine;

namespace Assets.Scripts.Models
{
    public class CardsInteractionModel
    {
        public GameObject CharacterCard { get; set; }
        public GameObject TargetEmptyCard { get; set; }
        public Vector3 OriginalCharacterCardPosition { get; set; }

        public int OriginRow { get; set; }
        public int OriginCol { get; set; }
        public int DestinationRow { get; set; }
        public int DestinationCol { get; set; }
        public GameObject GeneratedCard { get; internal set; }
    }
}
