﻿using System;

namespace Assets.Scripts.Game.Models
{
    public class Achievement
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public int MaxValue { get; set; }
    }
}
