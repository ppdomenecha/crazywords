﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Game.Models
{
    public class Student
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public List<StudentAchievemet> StudentAchievements { get; set; }
        public List<Word> StudentLearnedWords { get; set; }
    }
}
