﻿using System;

namespace Assets.Scripts.Game.Models
{
    public class StudentAchievemet
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public int CurrentValue { get; set; }
        public Achievement Achievement { get; set; }
    }
}
