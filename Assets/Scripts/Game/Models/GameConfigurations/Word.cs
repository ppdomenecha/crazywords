﻿using System;

namespace Assets.Scripts.Game.Models
{
    [Serializable]
    public class Word 
    {
        public Guid id;
        public string value;
        public string imageUrl;
        public Guid wordDatasetId;
    }
}
