﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Models;
using Assets.Scripts.Tracker;
using Assets.Scripts.Tracker.EventAttributes;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TableManager : MonoBehaviour
{
    public GameObject gameManagerObject;

    private int numRows;
    private int numCols;
    private bool isFirstMovement = true;

    private List<List<GameObject>> table;

    public static readonly int MAX_NUM_ROWS = 6;
    public static readonly int MAX_NUM_COLS = 13;
    public bool isTargetWordAchived;
    public const float Z_POSITION = 1.1f;

    // Start is called before the first frame update
    public void Start()
    {
        this.numCols = MAX_NUM_COLS;
        this.numRows = MAX_NUM_ROWS;

        table = new List<List<GameObject>>();
        this.isTargetWordAchived = false;

        GenerateGrid();
    }

    public bool IsValidMovement(GameObject targetCard, GameObject characterCard)
    {
        for (int i = 0; i < this.numRows; i++)
        {
            for (int j = 0; j < this.numCols; j++)
            {
                if (table[i][j].GetHashCode() == targetCard.GetHashCode())
                {
                    bool isValid = (i > 0 && table[i - 1][j].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                        (i < this.numRows - 1 && table[i + 1][j].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                        (j > 0 && table[i][j - 1].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                        (j < this.numCols - 1 && table[i][j + 1].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                        this.isFirstMovement;
                    if (!isValid)
                    {
                        this.gameManagerObject.GetComponent<GameManagerScript>().TrackEvent(new Assets.Scripts.Tracker.Event()
                        {
                            verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
                            objectAttribute = new ObjectEventAttribute()
                            {
                                id = "https://crazy-words.e-ucm.es/xapi/character/" + characterCard.GetComponentInChildren<BoardTile>().Value ?? "none",
                                definition = new ObjectDefinitionEventAttribute() { type = "https://crazy-words.e-ucm.es/xapi/seriousgames/interaction/move" }
                            },
                            resultAttribute = new Dictionary<string, object>
                            {
                                { "success" , false },
                                { "extensions", new Dictionary<string, object>
                                    {
                                        { "https://crazy-words.e-ucm.es/xapi/game/invalid-movement-reason", "There is no adjacent cards" },
                                        { "https://crazy-words.e-ucm.es/xapi/table/DestinationRow", i },
                                        { "https://crazy-words.e-ucm.es/xapi/table/DestinationCol", j }
                                    }
                                }
                            }
                        });
                    }
                    else { this.isFirstMovement = false; }
                    return isValid;
                }
            }
        }
        return false;
    }

    public void ManageCardsInteraction(CardsInteractionModel cardsInteractionModel)
    {
        for (int i = 0; i < this.numRows; i++)
        {
            for (int j = 0; j < this.numCols; j++)
            {
                if (table[i][j].GetHashCode() == cardsInteractionModel.TargetEmptyCard.GetHashCode())
                {
                    cardsInteractionModel.DestinationRow = i;
                    cardsInteractionModel.DestinationCol = j;
                    table[i][j].transform.GetChild(0).GetComponent<BoardTile>().IsDraggable = false;
                    CheckWordAchivement(i, j);
                    return;
                }
            }
        }
    }

    public void CheckWordAchivement(int row, int col)
    {
        string targetWord = this.gameManagerObject.GetComponent<GameManagerScript>().GetTargetWord().ToLower();
        string colValues = "";
        for(int i = 0; i < this.numRows; i++)
        {
            char? boardTileValue = 
                this.table[i][col].transform.GetChild(this.table[i][col].transform.childCount - 1).GetComponent<BoardTile>().Value;
            colValues += boardTileValue ?? ' ';
        }
        string rowValues = "";
        for (int j = 0; j < this.numCols; j++)
        {
            char? boardTileValue = 
                this.table[row][j].transform.GetChild(this.table[row][j].transform.childCount - 1).GetComponent<BoardTile>().Value;
            rowValues += boardTileValue ?? ' ';
        }

        if (colValues.ToLower().Contains(targetWord) || rowValues.ToLower().Contains(targetWord))
        {
            this.isTargetWordAchived = true;
        }
    }

    public List<List<char>> GetTableCharactersDistribution()
    {
        return table.Select(x => x.Select(y => y.transform.GetChild(y.transform.childCount - 1).GetComponent<BoardTile>().Value ?? ' ').ToList()).ToList();
    }

    private void GenerateGrid()
    {
        Vector3 lossyScale = gameObject.transform.lossyScale;
        var emptyCardResource = Resources.Load("Cards/EmptyCard");
        for (int i = 0; i < this.numRows; i++)
        {
            table.Add(new List<GameObject>());
            for (int j = 0; j < this.numCols; j++)
            {
                GameObject tile = new GameObject("BoardTile" + (i * this.numCols + j).ToString(), typeof(RectTransform))
                {
                    layer = (int)LayerEnum.UI
                };
                tile.transform.localScale = new Vector3(lossyScale.x, lossyScale.y, lossyScale.z);
                tile.transform.SetParent(transform);

                GameObject characterCard = (GameObject)Instantiate(emptyCardResource, tile.transform);
                characterCard.GetComponent<BoardTile>().Name = "EmptyCard";
                characterCard.GetComponent<BoardTile>().Value = null;
                characterCard.GetComponent<BoardTile>().IsDraggable = false;

                table[i].Add(tile);
            }
        }
    }

    public void RemarkValidPositions()
    {
        for (int i = 0; i < this.numRows; i++)
        {
            for (int j = 0; j < this.numCols; j++)
            {
                if (table[i][j].GetComponentInChildren<Image>().gameObject.layer == (int)LayerEnum.EMPTY_CARDS_LAYER || table[i][j].transform.GetChild(0).transform.Find("HelpMark") != null)
                {
                    bool isValid = (i > 0 && table[i - 1][j].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                            (i < this.numRows - 1 && table[i + 1][j].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                            (j > 0 && table[i][j - 1].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER) ||
                            (j < this.numCols - 1 && table[i][j + 1].GetComponentInChildren<Image>().gameObject.layer != (int)LayerEnum.EMPTY_CARDS_LAYER);
                    if (isValid)
                    {
                        table[i][j].transform.GetChild(0).transform.Find("HelpMark").gameObject.SetActive(true);
                    }
                }
            }
        }
    }

    public void RemoveAllValidPositionMarks()
    {
        for (int i = 0; i < this.numRows; i++)
        {
            for (int j = 0; j < this.numCols; j++)
            {
                if (table[i][j].GetComponentInChildren<Image>().gameObject.layer == (int)LayerEnum.EMPTY_CARDS_LAYER)
                {
                    table[i][j].transform.GetChild(0).transform.Find("HelpMark").gameObject.SetActive(false);
                }
            }
        }
    }
}
