﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class CutoutMaskUI : Image
{
    public override Material materialForRendering
    {
        get
        {
            Material copy = new Material(base.materialForRendering);
            copy.SetInt("_StencilComp", (int)CompareFunction.NotEqual);
            return copy;
        }
    }
}
