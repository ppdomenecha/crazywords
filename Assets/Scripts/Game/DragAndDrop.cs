﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Models;
using Assets.Scripts.Tracker.EventAttributes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour, IPointerDownHandler, IEndDragHandler, IDragHandler
{
    private RectTransform rectTransform;
    private Vector2 originalFixedPosition;
    public GameManagerScript gameManagerObject;

    public void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!gameManagerObject.CanDrag())
        {
            return;
        }
        originalFixedPosition = rectTransform.anchoredPosition;
        gameManagerObject.ActionDetected();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!gameManagerObject.CanDrag())
        {
            rectTransform.anchoredPosition = originalFixedPosition;
            return;
        }
        if (GetComponent<BoardTile>().IsDraggable)
        {
            rectTransform.position += new Vector3(eventData.delta.x, eventData.delta.y);
            gameManagerObject.ActionDetected();
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!gameManagerObject.CanDrag())
        {
            rectTransform.anchoredPosition = originalFixedPosition;
            return;
        }

        gameManagerObject.ActionDetected();
        Vector3 rayDirection = new Vector3(0, 0, -1);
        RaycastHit2D[] results = Physics2D.RaycastAll(transform.position, rayDirection);
        //Debug.DrawRay(transform.position, rayDirection, Color.red, Mathf.Infinity);
        GameObject targetCard = null;
        foreach (RaycastHit2D result in results)
        {
            if ((LayerEnum)result.collider.gameObject.layer == LayerEnum.EMPTY_CARDS_LAYER)
            {
                targetCard = result.collider.gameObject.transform.parent.gameObject;
            }
        }

        if (targetCard != null)
        {
            var cardsInteractionModel = new CardsInteractionModel()
            {
                CharacterCard = gameObject.transform.parent.gameObject,
                TargetEmptyCard = targetCard,
                OriginalCharacterCardPosition = originalFixedPosition
            };

            if (gameManagerObject.IsValidMovement(cardsInteractionModel))
            {
                gameManagerObject.ManageCardsInteraction(cardsInteractionModel);
                GetComponent<BoardTile>().IsDraggable = false;
            }
            else
            {
                gameManagerObject.ManageInvalidMovement();
                rectTransform.anchoredPosition = originalFixedPosition;
            }
        }
        else
        {
            gameManagerObject.ManageInvalidMovement();
            this.gameManagerObject.GetComponent<GameManagerScript>().TrackEvent(new Assets.Scripts.Tracker.Event()
            {
                verbAttribute = new VerbEventAttribute() { id = "http://adlnet.gov/expapi/verbs/interacted" },
                objectAttribute = new ObjectEventAttribute()
                {
                    id = "https://crazy-words.e-ucm.es/xapi/character/" + transform.GetComponent<BoardTile>().Value ?? "none",
                    definition = new ObjectDefinitionEventAttribute() { type = "https://crazy-words.e-ucm.es/xapi/seriousgames/interaction/move" }
                },
                resultAttribute = new Dictionary<string, object>
                {
                    { "success" , false },
                    { "extensions", new Dictionary<string, object>
                        {
                            { "https://crazy-words.e-ucm.es/xapi/game/invalid-movement-reason", "Destination is not an empty card" },
                        }
                    }
                }
            });
            rectTransform.anchoredPosition = originalFixedPosition;
        }

    }
}
