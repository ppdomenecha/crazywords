﻿using Assets.Scripts.Game.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuGameModeButtons : MonoBehaviour
{
    private readonly string GAME_SCENE_NAME = "2-Game";
    private readonly string STORY_MODE_SCENE_NAME = "5-StoryMode";

    public GameObject needsLearnWords;

    public void StoryGameMode()
    {
        SceneManager.LoadScene(STORY_MODE_SCENE_NAME);
    }

    public void FreeGameMode()
    {
        GoToGameScene();
    }

    public void ReviewGameMode()
    {
        WorldManager worldManager = WorldManager.GetDontDestroyOnLoadObjects().FirstOrDefault(x => x.GetComponent<WorldManager>() != null).GetComponent<WorldManager>();

        List<Word> availableWords = worldManager.studentGameConfiguration.Student.StudentLearnedWords;
        if (availableWords.Count > 0)
        {
            var random = new System.Random();
            int randomIndex = random.Next(0, availableWords.Count());
            worldManager.Gameplay = new Gameplay()
            {
                objective = availableWords.ElementAt(randomIndex),
                time = 100.0f,
                elapsedTime = 0,
                level = worldManager.GetGameConfigurationLevel(),
                score = 0,
                status = "InProgress", //"Abandoned", "Win", "Lose"
                events = new List<Assets.Scripts.Tracker.Event>()
            };
            worldManager.CreateServerGameplay();
        }
        else
        {
            this.needsLearnWords.SetActive(true);
        }
    }

    private void GoToGameScene()
    {
        SceneManager.LoadScene(GAME_SCENE_NAME);
    }
}
