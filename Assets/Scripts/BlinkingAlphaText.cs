﻿using System.Collections;
using UnityEngine;
using TMPro;

public class BlinkingAlphaText : MonoBehaviour
{
    public TMP_Text text;

    private bool sum = false;
    private float alpha = 1.0f;
    // Start is called before the first frame update
    public void Start()
    {
        StartBlinking();
    }

    private void StartBlinking()
    {
        StopCoroutine(Blink());
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        while (true)
        {
            if (sum)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
                sum = alpha < 1.0f;
                alpha += 0.05f;
            }
            else
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
                sum = alpha <= 0.4f;
                alpha -= 0.05f;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
}
