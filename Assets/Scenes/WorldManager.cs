﻿using Assets.Scripts.Game.Models;
using Assets.Scripts.Tracker;
using Assets.Scripts.Tracker.EventAttributes;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class WorldManager : MonoBehaviour
{
    private static WorldManager singletonInstance;
    private readonly string INITIAL_LOADING_SCENE_NAME = "0-InitialLoading";
    private readonly string MAIN_MENU_SCENE_NAME = "1-MainMenu";
    private readonly string GAME_SCENE_NAME = "2-Game";
    private IEventTracker eventTracker;
    private object sendRequestWithBodyResponse = null;

    //PARA DEV
    public static readonly string API_BASE_URL = "https://bsite.net/ProConrad/api";
    //public static readonly string API_BASE_URL = "http://schoolgame:44349/api";
    public string CurrentUserToken { get; set; }
    public string CurrentUserId { get; set; }
    public Gameplay Gameplay { get; set; }

    public StudentGameConfiguration studentGameConfiguration;
    public object user;
    public object userAchivements;
    public ConcurrentDictionary<string, Sprite> wordImages;

    public void Awake(){
        if (singletonInstance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singletonInstance = this;
            wordImages = new ConcurrentDictionary<string, Sprite>();
            DontDestroyOnLoad(gameObject);
            //this.eventTracker = new EventTrackerFile();
            //SetCurrentUser("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJhNTA3ZTQ1Ni1hNjcxLTQ1YmUtMzk2Yy0wOGQ5NDQ3NTVkNDciLCJpZCI6ImE1MDdlNDU2LWE2NzEtNDViZS0zOTZjLTA4ZDk0NDc1NWQ0NyIsImVtYWlsIjoic3R1ZGVudDFAeW9wbWFpbC5jb20iLCJyb2xlIjoiU1RVREVOVCIsIm5iZiI6MTYyNjIxNDc2MywiZXhwIjoxNjM0ODU0NzYzLCJpYXQiOjE2MjYyMTQ3NjMsImlzcyI6InNjaG9vbGdhbWUiLCJhdWQiOiJzY2hvb2xnYW1lIn0.CHBpfgu-FhFXLnYLNBDvAaiH0GXBYrfMtT0cnTuHnzk{SEPARADOR}a507e456-a671-45be-396c-08d944755d47");
        }
    }

    public static GameObject[] GetDontDestroyOnLoadObjects()
    {
        GameObject temp = null;
        try
        {
            temp = new GameObject();
            DontDestroyOnLoad(temp);
            Scene dontDestroyOnLoad = temp.scene;
            DestroyImmediate(temp);
            temp = null;

            return dontDestroyOnLoad.GetRootGameObjects();
        }
        finally
        {
            if (temp != null)
                DestroyImmediate(temp);
        }
    }

    public void SetCurrentUser(string data)
    {
        string[] separator = { "{SEPARADOR}" };
        this.CurrentUserToken = data.Split(separator, System.StringSplitOptions.RemoveEmptyEntries)[0];
        
        this.eventTracker = new EventTrackerDB(this.CurrentUserToken);

        this.CurrentUserId = data.Split(separator, System.StringSplitOptions.RemoveEmptyEntries)[1];
        StartCoroutine(this.GetConfiguration(API_BASE_URL + "/student-game-configurations/" + this.CurrentUserId, this.CurrentUserToken));
        SceneManager.LoadScene(INITIAL_LOADING_SCENE_NAME);
    }

    public void TrackGameEvent(Assets.Scripts.Tracker.Event eventToTrack)
    {
        eventToTrack.timestamp = JsonConvert.SerializeObject(DateTimeOffset.Now).Replace("\"", "");

        eventToTrack.actorAttribute = new ActorEventAttribute()
        {
            name = this.CurrentUserId,
            account = new ActorAccountEventAttribute()
            {
                name = this.CurrentUserId,
                homePage = "https://crazy-words.e-ucm.es/"
            }
        };

        eventToTrack.contextAttribute = new ContextEventAttribute()
        {
            contextActivities = new ContextActivitiesEventAttribute()
            {
                parent = new ContextActivitiesItemEventAttribute[]
                {
                    new ContextActivitiesItemEventAttribute()
                    {
                        id = "https://crazy-words.e-ucm.es/game/" + Gameplay.id
                    }
                },
                grouping = new ContextActivitiesItemEventAttribute[]
                {
                    new ContextActivitiesItemEventAttribute()
                    {
                        id = "https://crazy-words.e-ucm.es/dataset/" + Gameplay.objective.wordDatasetId
                    }
                },
                category = new ContextActivitiesItemEventAttribute[]
                {
                    new ContextActivitiesItemEventAttribute()
                    {
                        id = "https://w3id.org/xapi/seriousgames/v1.0"
                    }
                },
            }
        };

        eventToTrack.resultAttribute.Add("duration", DurationInSecondsToISO8601(this.Gameplay.elapsedTime));

        StartCoroutine(this.eventTracker.TrackEvent(eventToTrack));
    }

    public void CreateServerGameplay()
    {
        StartCoroutine(CreateServerGameplayCoroutine());
    }

    private IEnumerator CreateServerGameplayCoroutine()
    {
        yield return this.SendRequestWithBody(
            API_BASE_URL + "/gameplays",
            JsonConvert.SerializeObject(new
            {
                Id = Guid.Empty,
                Objective = Gameplay.objective,
                StartDate = DateTimeOffset.Now,
                Level = GetGameConfigurationLevel(),
                Score = 0,
                IsObjectiveAchieved = false
            }),
            "POST");
        
        Gameplay gamplayCreated = JsonUtility.FromJson<Gameplay>((string)this.sendRequestWithBodyResponse);
        Gameplay.id = gamplayCreated.id;
        this.sendRequestWithBodyResponse = null;
        
        GoToGameScene();
    }

    public IEnumerator EndServerGameplay()
    {
        yield return this.SendRequestWithBody(
            API_BASE_URL + "/gameplays/" + Gameplay.id,
            JsonConvert.SerializeObject(new
            {
                Id = Gameplay.id,
                Objective = Gameplay.objective,
                StartDate = Gameplay.startDate,
                EndDate = DateTimeOffset.Now,
                Level = Gameplay.level,
                Score = Gameplay.score,
                IsObjectiveAchieved = Gameplay.status == "Win",
                UserId = CurrentUserId
            }),
            "PUT"
        );
        yield return this.GetConfiguration(API_BASE_URL + "/student-game-configurations/" + this.CurrentUserId, this.CurrentUserToken);
    }

    public IEnumerator SendRequestWithBody(string url, string bodyJsonString, string method)
    {
        var request = new UnityWebRequest(url, method);
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Access-Control-Allow-Origin", "*");
        request.SetRequestHeader("Access-Control-Allow-Methods", "*");
        request.SetRequestHeader("Allowed-Hosts", "*");
        request.SetRequestHeader("Accept", "application/json, text/plain, */*");
        request.SetRequestHeader("Authorization", "Bearer " + this.CurrentUserToken);
        yield return request.SendWebRequest();

        while (!request.isDone) yield return new WaitForSeconds(0.1f);
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            Application.Quit();
        }
        else
        {
            this.sendRequestWithBodyResponse = request.downloadHandler.text;
        }
    }

    public int GetSuggestChangeLevelValue()
    {
        return this.studentGameConfiguration.SuggestChangeLevelValue;
    }

    public int GetGameConfigurationLevel()
    {
        return this.studentGameConfiguration.Level;
    }

    private IEnumerator GetConfiguration(string url, string token)
    {
        var request = new UnityWebRequest(url, "GET")
        {
            downloadHandler = new DownloadHandlerBuffer()
        };
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Access-Control-Allow-Origin", "*");
        request.SetRequestHeader("Access-Control-Allow-Methods", "*");
        request.SetRequestHeader("Allowed-Hosts", "*");
        request.SetRequestHeader("Accept", "application/json, text/plain, */*");
        request.SetRequestHeader("Authorization", "Bearer " + token);

        yield return request.SendWebRequest();

        while (!request.isDone) yield return new WaitForSeconds(0.1f);

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            Application.Quit();
        }
        else
        {
            studentGameConfiguration = JsonConvert.DeserializeObject<StudentGameConfiguration>(request.downloadHandler.text);
            //yield return new WaitForSeconds(2.0f);
            foreach (StudentGameConfigurationWordDataset wordDataset in studentGameConfiguration.StudentGameConfigurationWordDatasets)
            {
                foreach (Word word in wordDataset.WordDataset.Words)
                {
                    yield return DownloadImage(word.imageUrl);
                }
            }
            foreach (Word word in studentGameConfiguration.Student.StudentLearnedWords)
            {
                yield return DownloadImage(word.imageUrl);
            }
            SceneManager.LoadScene(MAIN_MENU_SCENE_NAME);
        }
    }

    private IEnumerator DownloadImage(string MediaUrl)
    {
        if (!wordImages.ContainsKey(MediaUrl))
        {
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
            yield return request.SendWebRequest();
            while (!request.isDone) yield return new WaitForSeconds(0.1f);
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
                Application.Quit();
            }
            else
            {
                var texture = DownloadHandlerTexture.GetContent(request);
                wordImages.TryAdd(MediaUrl, Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f));
            }
            yield return request.responseCode;
        }
    }

    private string DurationInSecondsToISO8601(int seconds)
    {
        return "P" + seconds + "S";
    }

    private void GoToGameScene()
    {
        SceneManager.LoadScene(GAME_SCENE_NAME);
    }
}
